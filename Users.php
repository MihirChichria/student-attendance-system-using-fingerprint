<?php
require("init.php");
if(!isset($_SESSION['logged-in'])){
    header('Location: log-in.php');
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>Students</title>
    <link rel="stylesheet" type="text/css" href="css/Users.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha1256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="js/jquery-2.2.3.min.js"></script>
    <script src="js/manage_users.js"></script>
</head>

<body>
    <style>
        .paginate_button:nth-child(2) a.page-link {
            background-color: #DE2567 !important;
            /*            color: aqua !important;*/
        }

    </style>
    <?php 
            include("header.php"); 
            // die(var_dump($_SESSION));
            if(isset($_SESSION['user_added'])){
                // die(var_dump($_SESSION));
                $user_added = $_SESSION['user_added'];
                // die(var_dump($user_added));
                if($user_added == "true"){
//                     die(var_dump($user_added));
   ?>
    <script>
        demo.showSwal('success-message', "", 'Student added successfully!');
    </script>
    <?php
            $_SESSION['user_added'] = "false";
                // die(var_dump($_SESSION));
            }
        }
        elseif(isset($_SESSION['user_edited'])){
            $user_edited = $_SESSION['user_edited'];
        if($user_edited == "true"){
    ?>
    <script>
        demo.showSwal('success-message', "", 'Student edited successfully!');
    </script>
    <?php
            $_SESSION['user_edited'] = "false";
        }
    }
    ?>
    <main>
        <section>
            <div class="wrapper">
                <?php
                    require_once("sidebar.php");
                ?>
                <div class="main-panel">
                    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top">
                        <div class="container-fluid">
                            <div class="navbar-wrapper">
                                <div class="navbar-minimize">
                                    <button id="minimizeSidebar" class="btn btn-just-icon btn-white btn-fab btn-round">
                                        <i class="material-icons text_align-center visible-on-sidebar-regular">more_vert</i>
                                        <i class="material-icons design_bullet-list-67 visible-on-sidebar-mini">view_list</i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </nav>

                    <!-- TABLE -->
                    <div class="content">
                        <div class="page-wrapper">
                            <div class="card shadow">
                                <div class="card-header card-header-rose card-header-icon">
                                    <div class="card-icon">
                                        <i class="material-icons">assignment</i>
                                    </div>
                                    <h4 class="card-title">Students</h4>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-no-bordered table-hover dataTable dtr-inline text-center" id="manage-student-datatable" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Enroll Number</th>
                                                    <th>Email</th>
                                                    <th>Gender</th>
                                                    <th>Dept</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</body>

</html>
