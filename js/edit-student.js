$(document).ready(function(){
    var id = $("#student_id").val();
    $.ajax({
        url: 'manage_users_conf.php',
        method: "POST",
        data: {
            "student_id": id,
            "page": "edit_student"
        },
        dataType: "json",
        success: function (data) {
            // alert("in success");
            // console.log(data[0].dept);
            $(".filter-option-inner-inner")[0].innerHTML = data[0].dept;
            if(data[0].gender == "Male"){
                $(".male")[0].checked = true;
                $(".male")[0].disabled = true;
                $(".female")[0].disabled = true;
            }else{
                $(".female")[0].checked = true;
                $(".female")[0].disabled = true;
                $(".male")[0].disabled = true;
            }
            $("#serialnumber")[0].readOnly = true;
            for(var i in data[0]){
                // console.log(i + " => ", data[0][i]);
                $("#"+i).val(data[0][i]);
            }
        }
    });
});