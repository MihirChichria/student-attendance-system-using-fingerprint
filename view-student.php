<?php
require("init.php");
if(!isset($_SESSION['logged-in'])){
    header('Location: log-in.php');
}
if(isset($_GET['id']))
    $id = $_GET['id'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" type="text/css" href="css/view-student.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha1256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="js/jquery-2.2.3.min.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Student</title>
</head>

<body>
    <?php
include("header.php");
?>

    <div class="wrapper">
        <?php
        include("sidebar.php");
    ?>
        <div class="main-panel">
            <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        <div class="navbar-minimize">
                            <button id="minimizeSidebar" class="btn btn-just-icon btn-white btn-fab btn-round">
                                <i class="material-icons text_align-center visible-on-sidebar-regular">more_vert</i>
                                <i class="material-icons design_bullet-list-67 visible-on-sidebar-mini">view_list</i>
                            </button>
                        </div>
                    </div>
                </div>
            </nav>

            <div class="container">
                <div class="row">
                   <input type="hidden" name="student_id" id="student_id" value="<?=$id?>">
                    <div class="col-md-12">
                        <div class="card" style="max-width:950px; margin:0 auto;">
                            <div class="card-header card-header-rose card-header-icon">
                                <div class="card-icon">
                                    <i class="material-icons">person</i>
                                </div>
                                <h4 class="card-title">View Student</h4>
                            </div>
                            <div class="card-body">
                                <div class="student-card">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-md-6">
                                               <label class="col-form-label">Username</label>
                                                <div class="form-group has-default bmd-form-group">
                                                    <input type="email" class="form-control" name="username" id="username" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                               <label class="col-form-label">Email</label>
                                                <div class="form-group has-default bmd-form-group">
                                                    <input type="email" class="form-control" name="email" id="email" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                               <label class="col-form-label">Serial Number</label>
                                                <div class="form-group has-default bmd-form-group">
                                                    <input type="text" class="form-control" name="serialnumber" id="serialnumber" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                               <label class="col-form-label">Gender</label>
                                                <div class="form-group has-default bmd-form-group">
                                                    <input type="email" class="form-control" name="gender" id="gender" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="col-form-label">Department</label>
                                                <div class="form-group has-default bmd-form-group">
                                                    <input type="email" class="form-control" name="dept" id="dept" disabled>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <script src="js/view-student.js"></script>
</body>

</html>
