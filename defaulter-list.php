<!DOCTYPE html>
<html>

    <head>
        <title>Defaulter List</title>
        <link rel="stylesheet" type="text/css" href="css/defaulter-list.css">
        <link
            href="https://fonts.googleapis.com/icon?family=Material+Icons"
            rel="stylesheet">
        <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha1256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
        <script src="js/jquery-2.2.3.min.js"></script>
    </head>

    <body>
        <?php include'header.php'; ?>
        <main>
            <section>
                <div class="wrapper">
                    <?php
                      require_once("sidebar.php");
                     ?>
                    <div class="main-panel">
                        <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
                            <div class="container-fluid">
                                <div class="navbar-wrapper">
                                    <div class="navbar-minimize">
                                        <button id="minimizeSidebar" class="btn btn-just-icon btn-white btn-fab btn-round">
                                            <i class="material-icons text_align-center visible-on-sidebar-regular">more_vert</i>
                                            <i class="material-icons design_bullet-list-67 visible-on-sidebar-mini">view_list</i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </nav>
                        <div class="container">
                            <div class="form-style-5 slideInDown animated">
                                <form method="POST" action="Export_Excel.php">
                                <!-- <input type="date" name="date_sel" id="date_sel"> -->

                                <!-- <button type="button" name="user_log" id="user_log">Select Date</button>
                                -->
                                <!-- <input type="submit" name="To_Excel" value="Export to Excel"> -->

                                <div class="row">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4">
                                        <div class="card">
                                            <div class="card-header card-header-rose card-header-text">
                                                <div class="card-icon">
                                                    <i class="material-icons">today</i>
                                                </div>
                                                <h4 class="card-title">Choose Month</h4>
                                            </div>
                                            <div class="card-body ">
                                            <div class="form-group">
                                                        <select class="selectpicker form=control" name="month" id="month" data-style="select-with-transition" title="month" data-size="12">
                                                            <option value="January">January</option>
                                                            <option value="February">February</option>
                                                            <option value="March" disabled>March</option>
                                                            <option value="April" disabled>April</option>
                                                            <option value="May" disabled>May</option>
                                                            <option value="June" disabled>June</option>
                                                            <option value="July" disabled>July</option>
                                                            <option value="August" disabled>August</option>
                                                            <option value="September" disabled>September</option>
                                                            <option value="October" disabled>October</option>
                                                            <option value="November" disabled>November</option>
                                                            <option value="December" disabled>December</option>
                                                        </select>
                                                        <button type="button" class="btn btn-rose btn-round btn-sm pull-right mt-3" name="user_log" id="select_month">Select Month
                                                        <div class="ripple-container"></div>
                                                        </button>
                                                        <div class="col-md-4" style="position: relative; top: 142px; left: 470px; z-index: 99999999;">
                                                            <button type="submit" class="btn btn-fill btn-rose btn-table" name="export_excel" value="Export to Excel">Export To Excel
                                                            </button>
                                                         </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="card">
                                <div class="card-header card-header-rose card-header-icon">
                                    <div class="card-icon">
                                        <i class="material-icons">assignment</i>
                                    </div>
                                    <h4 class="card-title">Defaulter List</h4>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Name</th>
                                                    <th class="text-center">Enrollment Number</th>
                                                    <th class="text-center">Month</th>
                                                    <!-- <th class="text-center">Date</th>
                                                    <th class="text-center">Time In</th>
                                                    <th class="text-center">Time Out</th> -->
                                                </tr>
                                            </thead>
                                            <tbody id="defaulter"></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <script src="js/defaulter-list.js"></script>
    </body>
</html>
        