<?php
    include("init.php");
    if(!isset($_SESSION['logged-in'])){
        header('Location: log-in.php');
    }
    if(isset($_GET['id'])){
        $student_id = $_GET['id'];
        // die($student_id);
    }

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" type="text/css" href="css/edit-student.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha1256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="js/jquery-2.2.3.min.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Student</title>
</head>

<body>
    <?php
include("header.php");
?>

    <div class="wrapper">
        <?php
        include("sidebar.php");
    ?>
        <div class="main-panel">
            <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        <div class="navbar-minimize">
                            <button id="minimizeSidebar" class="btn btn-just-icon btn-white btn-fab btn-round">
                                <i class="material-icons text_align-center visible-on-sidebar-regular">more_vert</i>
                                <i class="material-icons design_bullet-list-67 visible-on-sidebar-mini">view_list</i>
                            </button>
                        </div>
                    </div>
                </div>
            </nav>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card" style="max-width:950px; margin:0 auto;">
                            <div class="card-header card-header-rose card-header-icon">
                                <div class="card-icon">
                                    <i class="material-icons">person</i>
                                </div>
                                <h4 class="card-title">Edit Student</h4>
                            </div>
                            <div class="card-body">
                                <div class="student-card">
                                    <form method="POST" action="manage_users_conf.php" id="edit-student-form" name="edit-student-form">
                                        <div class="container-fluid">
                                            <div class="row ">
                                                <input type="hidden" id="student_id" value="<?=$student_id;?>">
                                                <div class="col-md-3">
                                                    <label for="username" class="d-block mt-3 margin-left-3 text-left">Enter Name</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" id="username" name="username">
                                                    </div>
                                                </div>
                                                <div class="col-md-3"></div>
                                                <div class="col-md-3">
                                                    <label for="email" class="d-block mt-3 margin-left-3 text-left">Enter Email</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" id="email" name="email">
                                                    </div>
                                                </div>
                                                <div class="col-md-3"></div>
                                                <div class="col-md-3">
                                                    <label for="serialnumber" class="d-block mt-3 margin-left-3 text-left">Enter Enroll Number</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" id="serialnumber" name="serialnumber">
                                                    </div>
                                                </div>
                                                <div class="col-md-3"></div>

                                                <div class="col-md-3">
                                                    <label for="department" class="d-block mt-3 margin-left-3 text-left">Select Department</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <select class="selectpicker form=control" name="dept" id="dept" data-style="select-with-transition" title="Dept" data-size="5">
                                                            <option value="CO">CO</option>
                                                            <option value="CM">CM</option>
                                                            <option value="EJ">EJ</option>
                                                            <option value="ET">ET</option>
                                                            <option value="EQ">EQ</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3"></div>


                                                <div class="col-md-3">
                                                    <label for="" class="d-block mt-3 margin-left-3 text-left">Select Gender</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="checkbox-radios">
                                                        <div class="form-check d-inline-block">
                                                            <label class="form-check-label">
                                                                <input class="form-check-input male" type="radio" name="gender" value="Male"> Male
                                                                <span class="circle">
                                                                    <span class="check"></span>
                                                                </span>
                                                            </label>
                                                        </div>
                                                        <div class="form-check d-inline-block">
                                                            <label class="form-check-label">
                                                                <input class="form-check-input female" type="radio" name="gender" value="Female"> Female
                                                                <span class="circle">
                                                                    <span class="check"></span>
                                                                </span>
                                                            </label>
                                                        </div>
                                                        <!-- <div class="form-group">
                                            <div class="col-md-6">
                                                <input type="radio" class="form-control d-inline" id="gender" name="gender">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="radio" class="form-control d-inline" id="gender" name="gender">
                                            </div>
                                        </div> -->
                                                    </div>
                                                    <div class="col-md-3"></div>
                                                </div>
                                            </div>
                                            <input type="submit" name="edit-student" id="edit-student" class="btn btn-fill btn-rose d-inline-block margin-left-3 margin-top-3" value="Submit">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <script src="js/edit-student.js"></script>
</body>

</html>
