<?php
include("init.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha1256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="js/jquery-2.2.3.min.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Student</title>
    <link rel="stylesheet" href="css/add-student.css">
</head>

<body>
    <?php
include("header.php");
?>

    <div class="wrapper">
        <?php
        include("sidebar.php");
    ?>
        <div class="main-panel">
            <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        <div class="navbar-minimize">
                            <button id="minimizeSidebar" class="btn btn-just-icon btn-white btn-fab btn-round">
                                <i class="material-icons text_align-center visible-on-sidebar-regular">more_vert</i>
                                <i class="material-icons design_bullet-list-67 visible-on-sidebar-mini">view_list</i>
                            </button>
                        </div>
                    </div>
                </div>
            </nav>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card" style="max-width:950px; margin:0 auto;">
                            <div class="card-header card-header-rose card-header-icon">
                                <div class="card-icon">
                                    <i class="material-icons">person</i>
                                </div>
                                <h4 class="card-title">Add New Student</h4>
                            </div>
                            <div class="card-body">
                                <div class="student-card">
                                    <form id="add-student-form" name="add-student-form">
                                        <div class="container-fluid">
                                            <div class="row ">
                                                <div class="col-md-3">
                                                    <label for="username" class="d-block mt-3 margin-left-3 text-left">Enter Name</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" id="username" name="username">
                                                    </div>
                                                </div>
                                                <div class="col-md-3"></div>
                                                <div class="col-md-3">
                                                    <label for="email" class="d-block mt-3 margin-left-3 text-left">Enter Email</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" id="email" name="email">
                                                    </div>
                                                </div>
                                                <div class="col-md-3"></div>
                                                <div class="col-md-3">
                                                    <label for="serialnumber" class="d-block mt-3 margin-left-3 text-left">Enter Enroll Number</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" id="serialnumber" name="serialnumber">
                                                    </div>
                                                </div>
                                                <div class="col-md-3"></div>

                                                <div class="col-md-3">
                                                    <label for="department" class="d-block mt-3 margin-left-3 text-left">Select Department</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <select class="selectpicker form=control" name="dept" id="dept" data-style="select-with-transition" title="Dept" data-size="5">
                                                            <option value="CO">CO</option>
                                                            <option value="CM">CM</option>
                                                            <option value="EJ">EJ</option>
                                                            <option value="ET">ET</option>
                                                            <option value="EQ">EQ</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3"></div>


                                                <div class="col-md-3">
                                                    <label for="" class="d-block mt-3 margin-left-3 text-left">Select Gender</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="checkbox-radios">
                                                        <div class="form-check d-inline-block">
                                                            <label class="form-check-label">
                                                                <input class="form-check-input" id="male" type="radio" name="gender" value="Male"> Male
                                                                <span class="circle">
                                                                    <span class="check"></span>
                                                                </span>
                                                            </label>
                                                        </div>
                                                        <div class="form-check d-inline-block">
                                                            <label class="form-check-label">
                                                                <input class="form-check-input" id="female" type="radio" name="gender" value="Female"> Female
                                                                <span class="circle">
                                                                    <span class="check"></span>
                                                                </span>
                                                            </label>
                                                        </div>
                                                        <!-- <div class="form-group">
                                            <div class="col-md-6">
                                                <input type="radio" class="form-control d-inline" id="gender" name="gender">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="radio" class="form-control d-inline" id="gender" name="gender">
                                            </div>
                                        </div> -->
                                                    </div>
                                                    <div class="col-md-3"></div>
                                                </div>
                                            </div>
                                            <input type="submit" name="add-student" id="add-student" class="btn btn-fill btn-rose d-inline-block margin-left-3 margin-top-3" value="Submit">
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- <div class="card-footer ">
                    <input type="submit" name="add-student" id="add-student" class="btn btn-fill btn-rose d-inline-block margin-left-3" value="Submit">
                </div> -->
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- <script>
    $("#add-student").on('click', function(){
        // alert("Called");
        $("#add-student-form").submit();
    });
</script>     -->
    <div id="fingerprint-input-modal">
        <h5 class="modal-title">
            Enroll Your Fingerprint
        </h5>
        <img src="img/fingerprint-transparent-gif-14.gif" alt="">
    </div>
    <div class="overlay"></div>
    <script src="js/add-student.js"></script>
</body>

</html>
