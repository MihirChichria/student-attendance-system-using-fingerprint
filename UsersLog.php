<?php
require("init.php");
if(!isset($_SESSION['logged-in'])){
    header('Location: log-in.php');
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>Student Logs</title>
    <link rel="stylesheet" type="text/css" href="css/userslog.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha1256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="js/jquery-2.2.3.min.js"></script>
    <script src="js/user_log.js"></script>
    <script>
        $(document).ready(function() {
            $.ajax({
                url: "user_log_up.php",
                type: 'POST',
                data: {
                    'select_date': 1
                }
            });
            setTimeout(function() {
                $.ajax({
                        url: "user_log_up.php",
                        type: 'POST',
                        data: {
                            'select_date': 0
                        }
                    })
                    .done(function(data) {
                        $('#users-log-table-data').html(data);
                    });
            });
        });

    </script>
</head>

<body>
    <?php include'header.php'; ?>
    <main>
        <section>
            <div class="wrapper">
                <?php
                      require_once("sidebar.php");
                     ?>
                <div class="main-panel">
                    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
                        <div class="container-fluid">
                            <div class="navbar-wrapper">
                                <div class="navbar-minimize">
                                    <button id="minimizeSidebar" class="btn btn-just-icon btn-white btn-fab btn-round">
                                        <i class="material-icons text_align-center visible-on-sidebar-regular">more_vert</i>
                                        <i class="material-icons design_bullet-list-67 visible-on-sidebar-mini">view_list</i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </nav>
                    <!--User table-->
                    <div class="container">
                        <div class="form-style-5 slideInDown animated">
                            <form method="POST" action="Export_Excel.php">
                                <div class="row">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4">
                                        <div class="card">
                                            <div class="card-header card-header-rose card-header-text">
                                                <div class="card-icon">
                                                    <i class="material-icons">today</i>
                                                </div>
                                                <h4 class="card-title">Choose Date</h4>
                                            </div>
                                            <div class="card-body ">
                                                <div class="form-group bmd-form-group is-filled">
                                                    <input type="text" class="form-control datepicker" value="" name="date_sel" id="date_sel">
                                                    <button type="button" class="btn btn-rose btn-round btn-sm pull-right mt-3" name="user_log" id="user_log">Select Date<div class="ripple-container"></div>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="position: relative;">
                                        <button type="submit" class="btn btn-fill btn-rose btn-table" name="To_Excel" value="Export to Excel">Export To Excel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card">
                            <div class="card-header card-header-rose card-header-icon">
                                <div class="card-icon">
                                    <i class="material-icons">assignment</i>
                                </div>
                                <h4 class="card-title">Students Logs</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Name</th>
                                                <th class="text-center">Serial Number</th>
                                                <th class="text-center">Fingerprint ID</th>
                                                <th class="text-center">Date</th>
                                                <th class="text-center">Time In</th>
                                                <th class="text-center">Time Out</th>
                                            </tr>
                                        </thead>
                                        <tbody id="users-log-table-data"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</body>
</html>
