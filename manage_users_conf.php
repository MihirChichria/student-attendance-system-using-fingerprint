<?php  
//Connect to database
require'init.php';
require'connectDB.php';
require'Config.php';
require'Database.php';

$c = new Config();
$db = new Database($c);


if(isset($_POST['page'])){
    if($_POST['page'] == "edit_student"){
        $id = $_POST["student_id"];
        $query = "SELECT * FROM users WHERE id = '{$id}'";
        $filteredData = $db->raw($query);
        echo json_encode($filteredData);
    }
}

if(isset($_POST['edit-student'])){
    $username = $_POST['username'];
    $email = $_POST['email'];
    $serialnumber = $_POST['serialnumber'];
    // die($email);
    // $gender = $_POST['gender'];
    $dept = $_POST['dept'];
    
    $data = [
        "username" => $username,
        "email" => $email,
        "dept" => $dept
    ];

    $user_id = $db->update("users", $data, "serialnumber={$serialnumber}");
    // die(var_dump($user_id));

    $_SESSION['user_edited'] = "true";
    // die(var_dump($_SESSION['user_added']));
    header("Location: Users.php");
}

if(isset($_POST['page']) && $_POST['page'] == 'defaulter-list'){
//    die("Inisde");
    $result = $db->raw("SELECT name,serialnumber,month FROM defaulter_list ORDER BY id", PDO::FETCH_ASSOC);
    $tbody = "";
    for($i=0;$i<count($result);$i++){
        $tbody .= "<tr class='text-center'>
                <td>".$result[$i]['name']."</td>
                <td>".$result[$i]['serialnumber']."</td>
                <td>".$result[$i]['month']."</td>
                </tr>";
    }
//    die(var_dump($tbody));
    echo $tbody;
}

if(isset($_POST['month_sel'])){
    // die(var_dump($_POST));
    $month = $_POST['month_sel'];
    // die($month);
    $result = $db->raw("SELECT name,serialnumber,month FROM defaulter_list WHERE month='{$month}'", PDO::FETCH_ASSOC);
    // die(var_dump($result[0]['month']));
    $tbody = "";
    for($i=0;$i<count($result);$i++){
        $tbody .= "<tr class='text-center'>
                <td>".$result[$i]['name']."</td>
                <td>".$result[$i]['serialnumber']."</td>
                <td>".$result[$i]['month']."</td>
                </tr>";
    }
    // die(var_dump($tbody));
    echo $tbody;
}

if(isset($_POST['log-in-button'])){
    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    
    $query = "SELECT COUNT(id) AS user_count, username FROM teachers WHERE username = '{$username}' AND email = '{$email}' AND password = '{$password}'";
    $result = $db->raw($query, PDO::FETCH_ASSOC);
    if($result[0]['user_count'] > 0){
        $_SESSION['logged-in'] = $result[0]['username'];
//        die($_SESSION['logged-in']);
        header('Location:users.php');
    }
}

if(isset($_POST['add-student'])){
    $username = $_POST['username'];
    $email = $_POST['email'];
    $serialnumber = $_POST['serialnumber'];
    $gender = $_POST['gender'];
    $dept = $_POST['dept'];
    
    $data = [
        "username" => $username,
        "email" => $email,
        "serialnumber" => $serialnumber,
        "gender" => $gender,
        "dept" => $dept,
        'add_fingerid' => 1,
        'fingerprint_select' => 1,
        'fingerprint_id'=>$serialnumber,
    ];

    $user_id = $db->insert("users", $data);
//     die(var_dump($user_id));

    $_SESSION['user_added'] = "true";
    // die(var_dump($_SESSION['user_added']));
//    header("Location: add-student.php");
    echo $user_id;
}
if(isset($_POST['page']) && $_POST['page'] == "view_student"){
    $id = $_POST["student_id"];
    $query = "SELECT * FROM users WHERE id = '{$id}'";
    $filteredData = $db->raw($query);
    echo json_encode($filteredData);
}
if(isset($_POST['page']) && $_POST['page'] == "delete_student"){
    $id = $_POST["student_id"];
    try{
        $db->beginTransaction();
//        $db->delete("users","id={$id}");
        $update['del_fingerid'] = 1;
        $db->update("users", $update, "id={$id}");
        $db->commit();
        $_SESSION['deleted'] = 'true';
        echo "true";
    }catch(Exception $e){
        $this->database->rollback();
        $_SESSION['deleted'] = 'false';
        echo "false";
    }
    
//    $_SESSION['deleted'] = 'true';
}
if(isset($_POST['page']) && $_POST['page'] == "select_all_records"){
    $searchParameter = $_POST['search']['value'] ?? null;
    $orderBy = $_POST['order'] ?? null;
    $start = $_POST['start'];
    $length = $_POST['length'];
    $draw = $_POST['draw'];
    
    $columns = ['Name', 'EnrollNumber', 'Email', 'Gender', 'Dept'];
    
    $query = "SELECT id, username as Name, serialnumber as EnrollNumber, email as Email, gender as Gender, dept as Dept FROM users WHERE deleted = 0 AND del_fingerid = 0";
    $filteredRowCountQuery = "SELECT * FROM users WHERE deleted = 0";
    $totalRowCountQuery = "SELECT COUNT(*) as total_count FROM users WHERE deleted = 0";
    
    if($searchParameter != null){
        $query .= " HAVING Name like '%{$searchParameter}%' AND Email like '%{$searchParameter}%'";
        $filteredRowCountQuery .= " WHERE username like '%{$searchParameter}%' AND email like '%{$searchParameter}%'";
    }

    if($orderBy !=null)
    {
        $query .= " ORDER BY {$columns[$orderBy[0]['column']]} {$orderBy[0]['dir']}";
    }

    if($length !=-1)
    {
        $query .=  " LIMIT {$start},{$length}";
    }
    
    $totalRowCountResult = $db->raw($totalRowCountQuery);
    $numberOfTotalRows = is_array($totalRowCountResult) ? $totalRowCountResult[0]->total_count : 0;
    
    $filteredRowCountResult = $db->raw($filteredRowCountQuery);
//    die(var_dump($filteredRowCountResult));
    $numberOfFilteredRows = is_array($filteredRowCountResult) ? count($filteredRowCountResult) : 0;

    $filteredData = $db->raw($query);
    $numberOfRowsToDisplay = is_array($filteredData) ? count($filteredData):0;
    
    $data = [];
        for($i=0;$i<$numberOfRowsToDisplay;$i++)
        {
            $subarray = [];
            
            $subarray[] = $filteredData[$i]->Name;
            $subarray[] = $filteredData[$i]->EnrollNumber;
            $subarray[] = $filteredData[$i]->Email;
            $subarray[] = $filteredData[$i]->Gender;
            $subarray[] = $filteredData[$i]->Dept;
            
            $subarray [] = <<<BUTTONS
            <div class="d-flex justify-content-center">
                <a href="#" class="btn btn-link btn-info btn-just-icon view" id='{$filteredData[$i]->id}'>
                        <i class="material-icons">dvr</i>
                </a>
                <a href="#" class="btn btn-link btn-warning btn-just-icon edit" id='{$filteredData[$i]->id}'>
                    <i class="material-icons">edit</i>
                </a>
                <a href="#" class="btn btn-link btn-danger btn-just-icon delete" id='{$filteredData[$i]->id}'>
                    <i class="material-icons">delete</i>
                </a>
            </div>
BUTTONS;
            $data[] = $subarray;
            
        }
    
    $output = array(
        "draw"=>$draw,
        "recordsTotal"=>$numberOfTotalRows,
        "recordsFiltered"=>$numberOfFilteredRows,
        "data"=>$data
    );
    
    echo json_encode($output);
}

if (isset($_GET['select'])) {

    $Finger_id = $_GET['Finger_id'];

    $sql = "SELECT fingerprint_select FROM users WHERE fingerprint_select=1";
    $result = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($result, $sql)) {
        echo "SQL_Error_Select";
        exit();
    }
    else{
        mysqli_stmt_execute($result);
        $resultl = mysqli_stmt_get_result($result);
        if ($row = mysqli_fetch_assoc($resultl)) {

            $sql="UPDATE users SET fingerprint_select=0";
            $result = mysqli_stmt_init($conn);
            if (!mysqli_stmt_prepare($result, $sql)) {
                echo "SQL_Error_Select";
                exit();
            }
            else{
                mysqli_stmt_execute($result);

                $sql="UPDATE users SET fingerprint_select=1 WHERE fingerprint_id=?";
                $result = mysqli_stmt_init($conn);
                if (!mysqli_stmt_prepare($result, $sql)) {
                    echo "SQL_Error_select_Fingerprint";
                    exit();
                }
                else{
                    mysqli_stmt_bind_param($result, "s", $Finger_id);
                    mysqli_stmt_execute($result);

                    echo "User Fingerprint selected";
                    exit();
                }
            }
        }
        else{
            $sql="UPDATE users SET fingerprint_select=1 WHERE fingerprint_id=?";
            $result = mysqli_stmt_init($conn);
            if (!mysqli_stmt_prepare($result, $sql)) {
                echo "SQL_Error_select_Fingerprint";
                exit();
            }
            else{
                mysqli_stmt_bind_param($result, "s", $Finger_id);
                mysqli_stmt_execute($result);

                echo "User Fingerprint selected";
                exit();
            }
        }
    } 
}
if (isset($_POST['Add'])) {
     
    $Uname = $_POST['name'];
    $Number = $_POST['number'];
    $Email= $_POST['email'];

    //optional
    $Timein = $_POST['timein'];
    $Gender= $_POST['gender'];

    //check if there any selected user
    $sql = "SELECT username FROM users WHERE fingerprint_select=1";
    $result = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($result, $sql)) {
      echo "SQL_Error";
      exit();
    }
    else{
        mysqli_stmt_execute($result);
        $resultl = mysqli_stmt_get_result($result);
        if ($row = mysqli_fetch_assoc($resultl)) {

            if (empty($row['username'])) {

                if (!empty($Uname) && !empty($Number) && !empty($Email)) {
                    //check if there any user had already the Serial Number
                    $sql = "SELECT serialnumber FROM users WHERE serialnumber=?";
                    $result = mysqli_stmt_init($conn);
                    if (!mysqli_stmt_prepare($result, $sql)) {
                        echo "SQL_Error";
                        exit();
                    }
                    else{
                        mysqli_stmt_bind_param($result, "d", $Number);
                        mysqli_stmt_execute($result);
                        $resultl = mysqli_stmt_get_result($result);
                        if (!$row = mysqli_fetch_assoc($resultl)) {
                            $sql="UPDATE users SET username=?, serialnumber=?, gender=?, email=?, user_date=CURDATE(), time_in=? WHERE fingerprint_select=1";
                            $result = mysqli_stmt_init($conn);
                            if (!mysqli_stmt_prepare($result, $sql)) {
                                echo "SQL_Error_select_Fingerprint";
                                exit();
                            }
                            else{
                                mysqli_stmt_bind_param($result, "sdsss", $Uname, $Number, $Gender, $Email, $Timein );
                                mysqli_stmt_execute($result);

                                echo "A new User has been added!";
                                exit();
                            }
                        }
                        else {
                            echo "The serial number is already taken!";
                            exit();
                        }
                    }
                }
                else{
                    echo "Empty Fields";
                    exit();
                }
            }
            else{
                echo "This Fingerprint is already added";
                exit();
            }    
        }
        else {
            echo "There's no selected Fingerprint!";
            exit();
        }
    }
}

if(isset($_POST['get-add_fingerid'])){
    $userid = $_POST['user-id'];
    $sql = "SELECT add_fingerid FROM users WHERE id='{$userid}'";
    $result = $db->raw($sql);
//    die(var_dump($result[0]->add_fingerid));
    echo $result[0]->add_fingerid;
}
//Add user Fingerprint
if (isset($_POST['Add_fingerID'])) {

    $fingerid = $_POST['fingerid'];

    if ($fingerid == 0) {
        echo "Enter a Fingerprint ID!";
        exit();
    }
    else{
        if ($fingerid > 0 && $fingerid < 128) {
            $sql = "SELECT fingerprint_id FROM users WHERE fingerprint_id=?";
            $result = mysqli_stmt_init($conn);
            if (!mysqli_stmt_prepare($result, $sql)) {
              echo "SQL_Error";
              exit();
            }
            else{
                mysqli_stmt_bind_param($result, "i", $fingerid );
                mysqli_stmt_execute($result);
                $resultl = mysqli_stmt_get_result($result);
                if (!$row = mysqli_fetch_assoc($resultl)) {

                    $sql = "SELECT add_fingerid FROM users WHERE add_fingerid=1";
                    $result = mysqli_stmt_init($conn);
                    if (!mysqli_stmt_prepare($result, $sql)) {
                      echo "SQL_Error";
                      exit();
                    }
                    else{
                        mysqli_stmt_execute($result);
                        $resultl = mysqli_stmt_get_result($result);
                        if (!$row = mysqli_fetch_assoc($resultl)) {
                            $sql = "INSERT INTO users (fingerprint_id, add_fingerid) VALUES (?, 1)";
                            $result = mysqli_stmt_init($conn);
                            if (!mysqli_stmt_prepare($result, $sql)) {
                              echo "SQL_Error";
                              exit();
                            }
                            else{
                                mysqli_stmt_bind_param($result, "i", $fingerid );
                                mysqli_stmt_execute($result);
                                echo "The ID is ready to get a new Fingerprint";
                                exit();
                            }
                        }
                        else{
                            echo "You can't add more than one ID each time";
                        }
                    }   
                }
                else{
                    echo "This ID is already exist!";
                    exit();
                }
            }
        }
        else{
            echo "The Fingerprint ID must be between 1 & 127";
            exit();
        }
    }
}
// Update an existance user 
if (isset($_POST['Update'])) {

    $Uname = $_POST['name'];
    $Number = $_POST['number'];
    $Email= $_POST['email'];

    //optional
    $Timein = $_POST['timein'];
    $Gender= $_POST['gender'];

    if ($Number == 0) {
        $Number = -1;
    }
    //check if there any selected user
    $sql = "SELECT * FROM users WHERE fingerprint_select=1";
    $result = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($result, $sql)) {
      echo "SQL_Error";
      exit();
    }
    else{
        mysqli_stmt_execute($result);
        $resultl = mysqli_stmt_get_result($result);
        if ($row = mysqli_fetch_assoc($resultl)) {

            if (empty($row['username'])) {
                echo "First, You need to add the User!";
                exit();
            }
            else{
                if (empty($Uname) && empty($Number) && empty($Email) && empty($Timein)) {
                    echo "Empty Fields";
                    exit();
                }
                else{
                    //check if there any user had already the Serial Number
                    $sql = "SELECT serialnumber FROM users WHERE serialnumber=?";
                    $result = mysqli_stmt_init($conn);
                    if (!mysqli_stmt_prepare($result, $sql)) {
                        echo "SQL_Error";
                        exit();
                    }
                    else{
                        mysqli_stmt_bind_param($result, "d", $Number);
                        mysqli_stmt_execute($result);
                        $resultl = mysqli_stmt_get_result($result);
                        if (!$row = mysqli_fetch_assoc($resultl)) {

                            if (!empty($Uname) && !empty($Email) && !empty($Timein)) {

                                $sql="UPDATE users SET username=?, serialnumber=?, gender=?, email=?, time_in=? WHERE fingerprint_select=1";
                                $result = mysqli_stmt_init($conn);
                                if (!mysqli_stmt_prepare($result, $sql)) {
                                    echo "SQL_Error_select_Fingerprint";
                                    exit();
                                }
                                else{
                                    mysqli_stmt_bind_param($result, "sdsss", $Uname, $Number, $Gender, $Email, $Timein );
                                    mysqli_stmt_execute($result);

                                    echo "The selected User has been updated!";
                                    exit();
                                }
                            }
                            else{
                                if (!empty($Timein)) {
                                    $sql="UPDATE users SET gender=?, time_in=? WHERE fingerprint_select=1";
                                    $result = mysqli_stmt_init($conn);
                                    if (!mysqli_stmt_prepare($result, $sql)) {
                                        echo "SQL_Error_select_Fingerprint";
                                        exit();
                                    }
                                    else{
                                        mysqli_stmt_bind_param($result, "ss", $Gender, $Timein );
                                        mysqli_stmt_execute($result);

                                        echo "The selected User has been updated!";
                                        exit();
                                    }
                                }
                                else{
                                    echo "The User Time-In is empty!";
                                    exit();
                                }    
                            }  
                        }
                        else {
                            echo "The serial number is already taken!";
                            exit();
                        }
                    }
                }
            }    
        }
        else {
            echo "There's no selected User to update!";
            exit();
        }
    }
}
// delete user 
if (isset($_POST['delete'])) {

    $sql = "SELECT fingerprint_select FROM users WHERE fingerprint_select=1";
    $result = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($result, $sql)) {
        echo "SQL_Error_Select";
        exit();
    }
    else{
        mysqli_stmt_execute($result);
        $resultl = mysqli_stmt_get_result($result);
        if ($row = mysqli_fetch_assoc($resultl)) {
            $sql="UPDATE users SET username='', serialnumber='', gender='', email='', time_in='', del_fingerid=1 WHERE fingerprint_select=1";
            $result = mysqli_stmt_init($conn);
            if (!mysqli_stmt_prepare($result, $sql)) {
                echo "SQL_Error_delete";
                exit();
            }
            else{
                mysqli_stmt_execute($result);
                echo "The User Fingerprint has been deleted";
                exit();
            }
        }
        else{
            echo "Select a User to remove";
            exit();
        }
    }
}
?>
