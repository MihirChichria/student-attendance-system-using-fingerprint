<div
    class="sidebar"
    data-color="rose"
    data-background-color="black"
    data-image="img/sidebar-1.jpg">
    <!-- Tip 1: You can change the color of the sidebar using: data-color="purple |
    azure | green | orange | danger" Tip 2: you can also add an image using
    data-image tag -->
    <div class="logo">
        <a href="#" class="simple-text logo-mini">
            <div class="sidebar-logo-top"><img src="icons/card_travel_white.png" alt=""></div>
        </a>
        <a href="#" class="simple-text logo-normal sidebar-logo-top-text">
            Attendance System
        </a>
        <!-- <i class="material-icons">keyboard_arrow_right</i> -->
    </div>
    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="img/faces/avatar.jpg"/>
            </div>
            <div class="user-info">
                <a data-toggle="collapse" href="#collapseExample" class="username">
                    <span>
                        <?php
                            echo $_SESSION['logged-in'];
                        ?>
                        <!-- <b class="caret"></b> -->
                    </span>
                </a>
                <!-- <div class="collapse" id="collapseExample">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <span class="sidebar-mini">
                                    MP
                                </span>
                                <span class="sidebar-normal">
                                    My Profile
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <span class="sidebar-mini">
                                    EP
                                </span>
                                <span class="sidebar-normal">
                                    Edit Profile
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <span class="sidebar-mini">
                                    S
                                </span>
                                <span class="sidebar-normal">
                                    Settings
                                </span>
                            </a>
                        </li>
                    </ul>
                </div> -->
            </div>
        </div>
        <ul class="nav">
            <!-- <li class="nav-item active "> <a class="nav-link"
            href="../examples/dashboard.html"> <i class="material-icons">people</i>
            <p>Students</p> </a> </li> -->
            <li class="nav-item ">
                <a class="nav-link" data-toggle="collapse" href="#studentOperations">
                    <i class="material-icons">people</i>
                    <p>
                        Students
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="studentOperations">
                    <ul class="nav">
                        <li class="nav-item ">
                            <a class="nav-link" href="add-student.php">
                                <span class="sidebar-mini">
                                    <i class="material-icons">add</i>
                                </span>
                                <span class="sidebar-normal">
                                    Add Student
                                </span>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link openEditModal" href="Users.php">
                                <span class="sidebar-mini">
                                    <i class="material-icons">build_outline</i>
                                </span>
                                <span class="sidebar-normal">
                                    Manage Student
                                </span>
                            </a>
                        </li>

                    </ul>
                </div>
            </li>
            <li class="nav-item ">
                <a class="nav-link" data-toggle="collapse" href="#viewStudents">
                    <i class="material-icons">remove_red_eye</i>
                    <p>
                        View
                        <b class="caret"></b>
                    </p>
                </a>

                <div class="collapse" id="viewStudents">
                    <ul class="nav">
                        <li class="nav-item" onclick="redirectToStudentsPage()">
                            <a class="nav-link redirectToUsers" data-toggle="collapse" href="Users.php">
                                <span class="sidebar-mini">
                                    <i class="material-icons">person_outline</i>
                                </span>
                                <span class="sidebar-normal">
                                    Students
                                </span>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="UsersLog.php">
                                <span class="sidebar-mini">
                                    <i class="material-icons">data_usage</i>
                                </span>
                                <span class="sidebar-normal">
                                    Students Log
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <script>
            function redirectToDefaulterPage(){
                window.location = "defaulter-list.php";
            }
            function redirectToStudentsPage(){
                window.location = "users.php";
            }
            </script>
            <li class="nav-item" onclick="redirectToDefaulterPage()">
                <a class="nav-link" data-toggle="collapse">
                    <i class="material-icons">assignment</i>    
                    <p>
                        Defaulter List
                    </p>
                </a>
            </li>
        </ul>
    </div>
</div>