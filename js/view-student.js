$(document).ready(function () {
    var id = $("#student_id").val();
    $.ajax({
        url: 'manage_users_conf.php',
        method: "POST",
        data: {
            "student_id": id,
            "page": "view_student"
        },
        dataType: "json",
        success: function (data) {
            for(var i in data[0]){
                console.log(i + " => ", data[0][i]);
                $("#"+i).val(data[0][i]);
            }
        }
    }); 
});
