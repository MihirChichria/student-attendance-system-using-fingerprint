//$(document).ready(function(){
//    $("#add-student").on('click', function(e){
//        e.preventDefault();
//        
//        $.ajax({
//             url: 'manage_user_conf.php',
//            method: 'POST',
//            data: {
//                $("#add-student).submit();
//            },
//            success: function(data){
//                console.log("Hey");
//                $(".fingerprint-input-modal").css("display", "block");
//            }
//        });
//    });
//});

$("#add-student-form").submit(function(e){
    e.preventDefault();
    var male = $("#male")[0].checked;
    var female = $("#female")[0].checked;
    var gender="";
    if(male){
        gender = "Male";
    }else if(female){
        gender = "Female";
    }
    var username = $("#username").val();
    var email = $("#email").val();
    var serialnumber = $("#serialnumber").val();
    
    var dept = $("#dept").val();
    $.ajax({
         url: 'manage_users_conf.php',
        method: 'POST',
        data: {
            'add-student': "true",
            "username": username,
            "email": email,
            "serialnumber": serialnumber,
            "gender": gender,
            "dept": dept    
        },
        dataType: "text",
        beforeSend: function() {
            $("#fingerprint-input-modal").css("display", "block");
            $(".overlay").css("display", "block");
        },
        success: function(data){
            console.log(data);
            setInterval(function(){
                $.ajax({
                    url: 'manage_users_conf.php',
                    method: 'POST',
                    data: {
                        'get-add_fingerid': 'true',
                        'user-id': data
                    },
                    dataType: "text",
                    success: function(data) {
                        if(data == 0)
                            window.location = "users.php";
                    }
                });
            }, 1000);
        },
        error: function(data){
            console.log(data);
        }
    });
});